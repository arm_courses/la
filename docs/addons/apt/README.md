# APT

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`apt-get` versus `apt` versus `aptitude`](#apt-get-versus-apt-versus-aptitude)
2. [`apt full-upgrade` versus `apt-get dis-upgrade`](#apt-full-upgrade-versus-apt-get-dis-upgrade)
3. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## `apt-get` versus `apt` versus `aptitude`

>apt - The main command-line package management tool
>aptitude - command-line and text-based interface (ncurses) for Apt
>>[debian wiki. PackageManagementTools](https://wiki.debian.org/PackageManagementTools)

## `apt full-upgrade` versus `apt-get dis-upgrade`

>The reason for the new name is that the "dist-upgrade" name was itself extremely confusing for many users: while it was named that because it was something you needed when upgrading between distribution releases, it sounded too much as though it was only for use in that circumstance, whereas in fact it's much more broadly applicable.
>>[apt full-upgrade versus apt-get dist-upgrade](https://askubuntu.com/questions/770135/apt-full-upgrade-versus-apt-get-dist-upgrade)

## Bibliographies

1. [Ubuntu Packages Search](https://packages.ubuntu.com/)
1. [Unable to locate package error on Ubuntu 20.04 Focal Fossa Linux](https://linuxconfig.org/unable-to-locate-package-error-on-ubuntu-20-04-focal-fossa-linux)
1. [How to enable/disable Universe, Multiverse and Restricted repository on Ubuntu 20.04 LTS Focal Fossa](https://linuxconfig.org/how-to-enable-disable-universe-multiverse-and-restricted-repository-on-ubuntu-20-04-lts-focal-fossa)
1. [debian wiki. PackageManagementTools](https://wiki.debian.org/PackageManagementTools)
1. [debian wiki. AptCLI](https://wiki.debian.org/AptCLI)
1. [debian wiki. Aptitude](https://wiki.debian.org/Aptitude)
1. [wikipedia. Aptitude](https://zh.wikipedia.org/zh-cn/Aptitude)
1. [apt full-upgrade versus apt-get dist-upgrade](https://askubuntu.com/questions/770135/apt-full-upgrade-versus-apt-get-dist-upgrade)
1. [Difference Between apt and apt-get Explained](https://itsfoss.com/apt-vs-apt-get-difference/)
1. [Linux中apt与apt-get命令的区别与解释](https://www.sysgeek.cn/apt-vs-apt-get/)
