# System Information

## Hardware Configuration

```bash {.line-numbers}
# cpu
/proc/cpu

# pci device


```

## Kernel and Distribution

```bash {.line-numbers}
uname               # print system information
lsb_release         # print distribution-specific information

```

## Resource Usage

```bash {.line-numbers}
# memory and swap
free
vmstat
/proc/swaps

# disk
df
du
```

## Application

```bash {.line-numbers}

```

## Bibliographies

1. [Linux 查看CPU信息，机器型号，内存等信息](https://my.oschina.net/hunterli/blog/140783)
