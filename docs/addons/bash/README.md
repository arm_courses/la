# Bash

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Special Characters](#special-characters)
    1. [Blank Character](#blank-character)
    2. [Metacharacter](#metacharacter)
    3. [Keywords/Reserved Words](#keywordsreserved-words)
    4. [Builtin Commands](#builtin-commands)
    5. [Special Variable](#special-variable)
    6. [Summary of Special Characters](#summary-of-special-characters)
        1. [Recommended Special Characters](#recommended-special-characters)
        2. [Deprecated special characters (recognized, but not recommended)](#deprecated-special-characters-recognized-but-not-recommended)
2. [命令替换vs变量替换](#命令替换vs变量替换)
3. [`$@` vs `$*`](#vs)
4. [各类括号的区别](#各类括号的区别)

<!-- /code_chunk_output -->

## Special Characters

>1. [Bash 中的特殊字符大全](https://linux.cn/article-5657-1.html)
>1. [Special characters](https://mywiki.wooledge.org/BashGuide/SpecialCharacters)
>1. [shell里面的奇葩字符](https://hellofrank.github.io/2020/01/13/shell%E9%87%8C%E9%9D%A2%E7%9A%84%E5%A5%87%E8%91%A9%E5%AD%97%E7%AC%A6/)
>1. [ABS. Chapter 3. Special Characters](https://tldp.org/LDP/abs/html/special-chars.html)

### Blank Character

space or tab.

### Metacharacter

>A character that, when unquoted, separates words. A metacharacter is a space, tab, newline, or one of the following characters: `‘|’`, `‘&’`, `‘;’`, `‘(’`, `‘)’`, `‘<’`, or `‘>’`.
>
>>[Bash Reference Manual. Definitions.](https://www.gnu.org/software/bash/manual/html_node/Definitions.html)

1. space
1. tab
1. newline
1. `|`
1. `&`
1. `;`
1. `(`
1. `)`
1. `>`
1. `<`

>（当没有被引号包围时）在`shell`中有特殊用途、由`shell`本身进行解析的字符称为元字符。
>>UNIX编程环境. 54-54.

<!--
1. `>`
1. `>>`
1. `<`
1. `|`
1. `<<string`
1. `*`
1. `?`
1. `{ccc}`
1. `;`
1. `&`
1. `` `...` ``

-->

### Keywords/Reserved Words

>A word that has a special meaning to the shell. Most reserved words introduce shell flow control constructs, such as for and while.
>>[Bash Reference Manual. Definitions.](https://www.gnu.org/software/bash/manual/html_node/Definitions.html)

```sh {.line-numbers}
$ compgen -k
```

```sh {.line-numbers}
if              # if分支keyword
then
else
elif
fi              # if分支的end-delimiter
case
esac
for
select
while
until
do
done
in
function
time
{
}
!               # 逻辑非
[[
]]
coproc
```

### Builtin Commands

```sh {.line-numbers}
$ compgen -b
```

```sh {.line-numbers}
.           # source命令的同义词，在本shell中执行
:           # 空命令
[           # test命令的同义词，最后一参数必须是]（以达到形式上方括号闭合的效果）
alias
bg
bind
break
builtin
caller
cd
command
compgen
complete
compopt
continue
declare
dirs
disown
echo
enable
eval
exec
exit
export
false
fc
fg
getopts
hash
help
history
jobs
kill
let
local
logout
mapfile
popd
printf
pushd
pwd
read
readarray
readonly
return
set
shift
shopt
source
suspend
test
times
trap
true
type
typeset
ulimit
umask
unalias
unset
wait
```

### Special Variable

### Summary of Special Characters

#### Recommended Special Characters

#### Deprecated special characters (recognized, but not recommended)

## 命令替换vs变量替换

1. 命令替换：将命令的`stdout`输出结果赋值给变量
1. 变量替换：根据变量的状态（是否为空、是否有定义等）返回值或修改变量值

变量为空指空字符串，变量未定义指从未定义或已删除

## `$@` vs `$*`

1. 当`$*`和`$@`不被双引号`" "`包围时，没有区别，都将所有参数拼接成一个字符串（以空格分隔）
1. 当`$*`和`$@`被双引号`" "`包围时，有区别
    1. `"$*"`将所有参数拼接成一个字符串（以空格分隔）
    1. `"$@"`将所有参数拼接成一个数组，每个参数是一个元素

>[Shell $*和$@的区别](http://c.biancheng.net/view/807.html)

---

```sh {.line-numbers}
#!/usr/bin/env bash

# test.sh

echo "print each param from \"\$*\""
for var in "$*"
do
    echo "$var"
done

echo "print each param from \"\$@\""
for var in "$@"
do
    echo "$var"
done
```

```sh {.line-numbers}
./test.sh a b c d 
```

## 各类括号的区别

>[Shell 参数扩展及各类括号在 Shell 编程中的应用](https://my.oschina.net/leejun2005/blog/368777#h2_6)
>[Linux shell中$(( ))、$( )、``与${ }的区别](https://www.cnblogs.com/chengd/p/7803664.html)
>[Bash技巧：介绍`test`、`[`、`[[`命令的用法和它们之间的区别](https://segmentfault.com/a/1190000022265700)

1. `()`：在`child shell`中执行
1. `$()`：取`()`的执行结果（不是返回值）
1. `[]`：条件表达式计算，结果为`true(1)`或`false(0)`
1. `$[]`：`$(())`过去形式，现已不建议使用
1. `[[]]`：`[]`的功能提升，同时支持基本的数值运算
1. `$[[]]`：不存在
1. `(())`：表达式计算，结果`非0`或`非空`表示`true`、`0`或`空`表示`false`
1. `$(())`：表达式扩展，取`(())`的计算结果
1. `{}`：
    1. 正则表达式或？
    1. 子命令组
1. `${}`：变量替换（`parameter expansion`）

❗ ❗ ❗ ❗ ❗ ❗

1. `[[]]`, `(())`, `$(())`是`bash`扩展，不是`POSIX`标准
1. `[`是命令，对分隔符（空白字符）有严格要求
1. `{`作为子命令组时，由于`{`是保留字，因此，`{`后、`}`前必须有`metacharacter`
1. 🌟 `[]`和`[[]]`将其中的内容均识别为字符串，如：`0`不是空字符串，所以，`[ 0 ]`的结果是`true`
1. 🌟 `()`将其中的标识符识别为命令，如：`0`和`1`不是命令，所以，`(0)`或`(1)`将报错
1. 🌟 `(())`将其中的标识符识别为变量（并取变量的值），如：`true`是命令，不是变量，所以，`((true))`的结果是`false`

❗ ❗ ❗ ❗ ❗ ❗
