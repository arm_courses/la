# Builtin Command

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`:`冒号](#冒号)
2. [`[`左方括号](#左方括号)

<!-- /code_chunk_output -->

## `:`冒号

`:`除了是`builtin command`外，还常用来做路径分隔符、数据字段分隔符、变量替换中的字符等

`:`作为`builtin command`时表达空命令，即什么都不做、起到占位的作用

## `[`左方括号

❗ `[`是一个命令，后面紧接的是选项或参数，因此，需要使用空格将命令和参数进行分隔，`]`是一个参数，因此也需要使用空格与其他参数进行分隔 ❗

>[Bash技巧：介绍`test`、`[`、`[[`命令的用法和它们之间的区别](https://segmentfault.com/a/1190000022265700)
