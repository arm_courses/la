# Compound Commands复合命令

## 什么是复合命令

>Compound commands are the shell programming language constructs. Each construct begins with a reserved word or control operator and is terminated by a corresponding reserved word or operator.
>
>复合命令是shell编程语言的一类构造。每一个构造由保留字或控制运算符开头，并由相对应的保留字或控制运算符结束。
>
>>[3.2.5 Compound Commands](https://www.gnu.org/software/bash/manual/html_node/Compound-Commands.html)

## 复合命令有哪些

>A compound command is one of the following.  In most cases a list in a command's description may be separated from the rest of the command by one or more newlines, and may be followed by a newline in place of a semicolon.
>
>`(list)`
>list  is  executed  in a subshell environment (see COMMAND EXECUTION ENVIRONMENT below).  Variable assignments and builtin commands that affect the shell's environment do not remain in effect after the command completes.  The return status is the exit status of list.
>
>`{ list; }`
>list is simply executed in the current shell environment.  list must be terminated with a newline or semicolon.  This is known as a group command.  The return status is the exit status of list.  Note that unlike the metacharacters ( and ), { and } are reserved words and must occur where a reserved word is permitted to be recognized.  Since they do not cause a word break, they must be separated from list by whitespace or another shell metacharacter.
>
>`((expression))`
The expression is evaluated according to the rules described below under ARITHMETIC EVALUATION.  If the value of the expression is non-zero, the return status is 0; otherwise the return status is 1.  This is exactly equivalent to let "expression".
>
>`[[ expression ]]`
Return  a  status of 0 or 1 depending on the evaluation of the conditional expression expression.  Expressions are composed of the primaries described below under CONDITIONAL EXPRESSIONS.  Word splitting and pathname expansion are not performed on the words between the [[ and ]]; tilde expansion, parameter and variable expansion, arithmetic expansion, command substitution, process substitution, and quote removal are performed.  Conditional operators such as -f must be unquoted to be  recognized  as primaries.
>
>>`man bash`
