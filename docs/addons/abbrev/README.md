# Linux Abbreviations and Acronyms

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Environment Variables](#environment-variables)
2. [Filesystem Hierarchy](#filesystem-hierarchy)
3. [Filename](#filename)
4. [Commands](#commands)
5. [Service](#service)
6. [Software](#software)
7. [Programming Language](#programming-language)
8. [VIM](#vim)
9. [ETC](#etc)
10. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## Environment Variables

1. `ENV` = `ENVironment`, `environment`
1. `LC` = `Locale Category`, `locale category`
1. `PS` = `Prompt String`, `prompt string`
1. `IFS` = `Internal Field Separators`, `internal field separators`

## Filesystem Hierarchy

1. `/usr` = `User System Resources`, `user system resources`, originally intended was short of `user`
1. `/bin` = `BINaries`, `binaries`
1. `/dev` = `DEVices`, `devices`
1. `/etc` = `ETCetera`/`Editable Text Config`, `etcetera`/`editable text config`
1. `/lib` = `LIBrary`, `library`
1. `/proc` = `PROCesses`, `processes`
1. `/sbin` = `Superuser BINaries`, `superuser binaries`
1. `/tmp` = `TeMPorary`, `temporary`
1. `/var` = `VARiable`, `variable`
1. `/mnt` = `MouNT`, `mount`
1. `/opt` = `OPTional`, `optional`
1. `/srv` = `SeRVer`, `server`

## Filename

1. `*_lsb` = `Linux Standard Base`, `linux standard base`
1. `*rc` = `Run Commands`, `run commands`
1. `*.a` = `Archive`, `archive`
1. `*.so` = `Shared Object`, `shared object`
1. `fstab` = `FileSystem TABle`
1. `m4` = `Macro processor version 4`

## Commands

### A

1. `apt` = `Advanced Packaging Tool`
1. `ar` = `ARchiver`, `archiver`
1. `awk` = `AWKward`, `awkward`

### B

1. `bash` = `Bourne-Again SHell`, `bourne-again shell`
1. `bc` = `Basic Calculator`/`Better Calculator`
1. `bg` = `BackGround`, `background`

### C

1. `cal` = `CALendar`, `calendar`
1. `cat` = `CATenate`, `catenate`
1. `cd` = `Change Directory`
1. `chown` = `CHange OWNer`, `change owner`
1. `chgrp` = `CHange GRouP`, `change group`
1. `chsh` = `CHange SHell`, `change shell`
1. `chmod` = `CHange MODe`, `change mode`
1. `cmp` = `CoMPare`, `compare`
1. `cp` = `CoPy`, `copy`
1. `cpio` = `CoPy In and Out`, `copy in and out`

### D

1. `df` = `Disk Free`, `disk free`
1. `du` = `Disk Usage`, `disk usage`
1. `dmesg` = `Display MESsaGe`, `display message`
1. `dc` = `Desk Calculator`, `desk calculator`
1. `dd` = `Disk Dump`, `disk dump`
1. `df` = `Disk Free`, `disk free`
1. `diff` = `DIFFerence`, `difference`
1. `dmesg` = `Diagnostic MESsaGe`, `diagnostic message`
1. `du` = `Disk Usage`

### E

1. `ed` = `EDitor`, `editor`
1. `egrep` = `Extended GREP`, `extended grep`
1. `elm` = `ELectronic Mail`, `electronic mail`
1. `emacs` = `Editor MACroS`, `editor macros`
1. `eval` = `EVALuate`, `evaluate`
1. `ex` = `EXtended`, `extended`
1. `exec` = `EXECute`, `execute`

### F

1. `fg` = `ForeGround`, `foreground`
1. `fgrep` = `Fixed GREP`, `fixed grep`
1. `fmt` = `ForMaT`, `format`
1. `fsck` = `File System ChecK`, `file system check`

### G

1. `gnome` = `Gnu Network Object Model Environment`, `gnu network object model environment`
1. `grep` = `g/regular-expression/p`, `Global Regular Expression Print`, `global regular expression print`
1. `gpg` = `GNU Privacy Guard`, `gun privacy guard`

### H

### I

1. `insmod` = `Install module`
1. `ipcalc`  = `IP Calculate`

### J

1. `joe` = `Joe's Own Editor`, `joe's own editor`

### K

1. `ksh` = `Korn SHell`, `korn shell`
1. `kde` = `K Desktop Environment`, `k desktop environment`

### L

1. `ls` = `LiSt`, `list`
1. `lsblk` = `LiSt BLocK devices`, `list block devices`
1. `lsof` = `LiSt Open Files`, `list open files`
1. `ldd` = `List Dynamic Dependencies`, `list dynamic dependencies`
1. `lame` = `Lame Ain't an MP3 Encoder`, `lame ain't an mp3 encoder`
1. `lex` = `LEXical analyser`, `lexical analyser`
1. `ln` = `LiNk`, `link`
1. `lpr` = `Line PRint`, `line print`

### M

1. `man` = `MANual pages`, `manual pages`
1. `mawk` = `Mike Brennan's AWK`, `mkie brennan's awk`
1. `mc` = `Midnight Commander`, `midnight commander`
1. `mkfs` = `MaKe FileSystem`, `make filesystem`
1. `mknod` = `MaKe NODe`, `make node`
1. `motd` = `Message of The Day`, `message of the day`
1. `mozilla` = `MOsaic GodZILLa`, `mosaic godzilla`
1. `mtab` = `Mount TABle`, `mount table`
1. `mv` = `MoVe`, `move`

### N

1. `nano` = `Nano's ANOther editor`, `nano's another editor`
1. `nawk` = `New AWK`, `new awk`
1. `nl` = `Number of Lines`, `number of lines`
1. `nm` = `NaMes`, `names`
1. `nohup` = `No HangUP`, `no hangup`
1. `nroff` = `New ROFF`, `new roff`

### O

1. `od` = `Octal Dump`, `octal dump`

### P

1. `ps` = `Processes Status`, `processes status`
1. `passwd` = `PASSWorD`, `password`
1. `pg` = `PaGer`, `pager`
1. `pico` = `PIne's message COmposition editor`, `pine's message composition editor`
1. `pine` = `Program for Internet News & Email`, `program for internet news & email` = `PINE Is Not Email`, `pine is not email`
1. `ping` = `Packet InterNet Grouper`, `packet internet grouper`
1. `pirntcap` = `PRINTer CAPability`, `printer capability`
1. `popd` = `POP Directory`, `pop directory`
1. `pr` = `for PRint`, `for print`
1. `printf` = `PRINT Formatted`, `print formatted`
1. `pty` = `Pseudo TtY`, `pseudo tty`
1. `pushd` = `PUSH Directory`, `push directory`
1. `pwd` = `Print Working Directory`, `print working directory`

### Q

1. `qt` = `Quasar technologies Toolkit`, `quasar technologies toolkit`

### R

1. `rmmod` = `ReMove MODule`, `remove module`
1. `rev` = `REVerse`, `reverse`
1. `rm` = `ReMove`, `remove`
1. `rn` = `Read News`, `read news`
1. `roff` = `RunOFF`, `runoff`
1. `rpm` = `RPM Package Manager`, `rpm package manager` = `RedHat Package Manager`, `redhat package manager`
1. `rsh` = `Remote SHell`, `remote shell`
1. `rlogin` = `Remote LOGIN`, `remote login`
1. `rxvt` = `ouR XVT`, `our xvt`

### S

1. `su` = `Switch User`/`Substitute User`, `switch user`/`substitute user`
1. `sudo` = `SuperUser DO`, `superuser do`
1. `sed` = `Stream EDitor`, `steam editor`
1. `seq` = `SEQuence`, `sequence`
1. `shar` = `SHell ARchive`, `shell archive`
1. `slrn` = `S-Lang rn`, `s-lang rn`
1. `ssh` = `Secure SHell`, `secure shell`
1. `ssl` = `Secure Sockets Layer`, `secure sockets layer`
1. `stty` = `Set TTY`, `set tty`
1. `shopt` = `SHell OPTion`, `shell option`

### T

1. `tar` = `Tape ARchive`, `tape archive`
1. `tcsh` = `Tenex C SHell`, `tenex c shell`
1. `telnet` = `TErminaL over Network`, `terminal over network`
1. `termcap` = `TERMinal CAPability`, `terminal capability`
1. `terminfo` = `TERMinal INFOrmation`, `terminal information`
1. `tr` = `TRanslate`, `translate`
1. `troff` = `Typesetter new ROFF`, `typesetter new roff`
1. `tsort` = `Topological SORT`, `topological sort`
1. `tty` = `TeleTYpewriter`, `teletypewriter`
1. `twm` = `Tom's Window Manager`, `tom's window manager`
1. `tz` = `TimeZone`, `timezone`

### U

1. `uname` = `Unix NAME`, `unix name`
1. `umount` = `UnMOUNT`, `unmount`
1. `udev` = `Userspace DEVice`, `userspace device`
1. `ulimit` = `User's LIMIT`, `user's limit`
1. `umask` = `User's MASK`, `user's mask`
1. `uniq` = `UNIQue`, `unique`

### V

1. `vi` = `VIsual`/`Very Inconvenient`, `visual`/`very inconvenient`
1. `vim` = `Vi IMproved`, `vi improved`

### W

1. `wall` = `Write ALL`, `write all`
1. `wc` = `Word Counting`, `word counting`
1. `wine` = `WINE Is Not an Emulator`, `wine is not an emulator`

### X

1. `xargs` = `eXtended ARGuments`, `extended arguments`
1. `xdm` = `X Display Manager`, `x display manager`
1. `xlfd` = `X Logical Font Description`, `x logical font description`
1. `xmms` = `X Multimedia System`, `x multimedia system`
1. `xrdb` = `X Resources DataBase`, `x resources database`
1. `xwd` = `X Window Dump`, `x window dump`

### Y

1. `yacc` = `yet another compiler compiler`, `yet another compiler compiler`
1. `YaST` = `Yet Another Setup Tool`, `yet another setup tool`

### Z

1. `zsh` = `Zhong shao SHell`, `zhong shao shell`, "Zhong Shao" is a teaching assistant while the author(Paul Falstad) as a student at Princeton University

## Service

1. `daemon` = `Disk And Execution MONitor`, `disk and execution monitor`

## Software

1. `GRUB` = `GRand Unified Bootloader`, `grand unified bootloader`
1. `LILO` = `LInux LOader`, `linux loader`
1. `apache` = `A PAtCHy sErver`, `a patchy server`
1. `cups` = `Common Unix Printing System`, `common unix printing system`
1. `cvs` = `Concurrent Versions System`/`Concurrent Versioning System`, `concurrent versions system`/`concurrent versioning system`
1. `fvwm` = `F*** Virtual Window Manager`, `f*** virtual window manager`
1. `svn` = `SubVersioN`, `subversion`

## Programming Language

1. `as` = `ASsembler`, `assembler`
1. `ajax` = `Asynchronous Javascript And Xml`, `asynchronous javascript and xml`
1. `cobra` = `Common Object Request Broker Architecture`, `common object request broker architecture`
1. `cpp` = `C Pre Processor`, `c pre processor`
1. `cpp` = `C Plus Plus`, `c plus plus`
1. `elf` = `Extensible Linking Format`, `extensible linking format`
1. `jsr` = `Java Specification Request`, `java specification request`
1. `json` = `JavaScript Object Notation`, `javascript object notation`
1. `lisp` = `LISt Processing`, `list processing` = `Lots of Irritating Superfluous Parentheses`, `lots of irritating superfluous parentheses`
1. `perl` = `Practical Extraction and Report Language`, `practical extraction and report language` = `Pathologically Eclectic Rubbish Lister`, `pathologically eclectic rubbish lister`
1. `php` = `Personal Home Page tools`, `personal home page tools` = `PHP Hypertext Preprocessor`, `php hypertext preprocessor`
1. `sql` = `Structured Query Language`, `structured query language`
1. `tcl` = `Tool Command Language`, `tool command language`
1. `tk` = `ToolKit`, `toolkit`
1. `xml` = `eXtensible Markup Language`, `extensible markup language`

## VIM

1. `a` = `Append`, `append`
1. `b` = `Back`, `back`
1. `c` = `Change`, `change`
1. `d` = `Delete`, `delete` = `Down`, `down`
1. `e` = `End`, `end`
1. `f` = `Find`, `find` = `Forward`, `forward`
1. `g` = `Goto`, `goto`
1. `h` = `Home`, `home`
1. `i` = `Insert`, `insert`
1. `j` = `Join`, `join`
1. `m` = `Mark`, `mark`
1. `n` = `Next`, `next`
1. `o` = `abOve`, `above`
1. `p` = `Paste`, `paste`
1. `q` = `Quit`, `quit`
1. `r` = `Replace`, `replace` = `Revert`, `revert`
1. `s` = `Substitute`, `substitute`
1. `t` = `Till`, `till`
1. `u` = `Undo`, `undo` = `Up`, `up`
1. `v` = `Visual`, `visual`
1. `w` = `Word`, `word`
1. `y` = `Yank`, `yank`

## ETC

1. `hal` = `Hardware Abstraction Layer`, `hardware abstraction layer`
1. `fd` = `file descriptors`
1. `comm` = `common`
1. `vt` = `Video Terminal`

## Bibliographies

1. [Linux Dictionary](https://tldp.org/LDP/Linux-Dictionary/)
1. [UNIX 缩写习惯](https://gnu-linux.readthedocs.io/zh/latest/Chapter04/30_unix.abbreviation.html)
1. [The Linux Acronym List](http://www.linfo.org/acronym_list.html)
1. [Abbreviations and acronyms database](https://abbreviations.woxikon.com)
