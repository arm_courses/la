# Linux的基本使用Basic in Linux

## 实验基本信息

1. 实验性质：综合性
1. 实验学时：4
1. 实验目的：
    1. 掌握`Ubuntu`的安装
    1. 熟悉`Ubuntu`的虚拟控制台
    1. 理解`shell`命令
    1. 掌握`SSH`连接`Ubuntu`
    1. 了解`Vim`
1. 实验内容与要求：
    1. 实验内容01：安装`Ubuntu Server`
    1. 实验内容02：在多个虚拟控制台切换
    1. 实验内容03：通过`SSH`连接`Ubuntu`
    1. 实验内容04：使用`vim`新建编辑一个文本文件并保存
1. 实验条件：
    1. 系统环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`
    1. 软件环境：`VirtualBox`或其他虚拟机

## 实验前置条件

1. 已安装`VirtualBox`或其他虚拟机
1. `Ubuntu Server`镜像文件，建议`16.04`或以上版本
1. 建议具备`Internet`连接

## 实验步骤

### 实验内容01：安装`Ubuntu Server`

1. 安装`Ubuntu Server`
1. 安装过程中勾选`Install OpenSSH server`
1. 安装过程创建的账号按 **"`{姓名拼音全拼}_{学号后两位}`"（如`zhangsan_68`）命名** :exclamation:
1. 安装完毕后重启登录

### 实验内容02：在多个虚拟控制台切换

1. 通过`<ctrl> + <alt> + <F(n)>`（F(n)为F1~F6）在三个或以上虚拟控制台中切换并登录
1. 分别运行`w`、`who`、`whoami`、`who am i`、`who ab`、`who a b`、`who -m`，观察各个命令的输出结果

### 实验内容03：通过`SSH`连接`Ubuntu`

1. 使用`poweroff`命令关闭`Ubuntu`虚拟机
1. 在`VirtualBox`中将`Ubuntu`虚拟机的网络模式修改为`Bridged Adapter`并保存（`Settings>>>Network`）
1. 启动`Ubuntu`虚拟机并登录
1. 通过`apt policy openssh-server`查看是否已安装`openssh-server`，如未安装，通过`apt install openssh-server`安装
1. 通过`ip addr show`或`hostname -I`查询`Ubuntu`虚拟机的IP地址
1. 下载安装`PuTTY`或其他`SSH`客户端
1. 通过`PuTTY`或其他`SSH`客户端远程连接`Ubuntu`虚拟机
1. 在`Ubuntu`虚拟机中至少登录一个虚拟控制台
1. 分别运行`w`、`who`、`whoami`、`who am i`，观察各个命令的输出结果

### 实验内容04：使用`vim`新建编辑一个文本文件并保存

1. 登录虚拟控制台或`SSH`远程终端
1. 运行`vi`
1. 从`normal mode`进入`insert mode`，输入不少于3行的字符
1. 从`insert mode`退出到`normal mode`，进行多行间的上下左右移动、删除字符、删除单词、删除整行、复印整行、粘贴整行等操作
1. 从`normal mode`进入`command-line mode`，将文本保存为文件名为 **"`{姓名拼音全拼}_{学号后两位}`"命名（如`zhangsan_68`）** :exclamation:
1. 退出`vim`
1. 分别通过`cat`、`more`、`less`查看刚编辑的文本文件
1. 分别运行`cat /proc/vmstat`、`more /proc/vmstat`、`less /proc/vmstat`观察`cat`、`more`、`less`3个命令的相同点与不同点
