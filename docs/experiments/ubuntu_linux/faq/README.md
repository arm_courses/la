# FAQ

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [有哪些方式可以获得`Linux`的实验环境❓](#有哪些方式可以获得linux的实验环境)
2. [`VirtualBox`如何进行虚拟机迁移❓](#virtualbox如何进行虚拟机迁移)

<!-- /code_chunk_output -->

## 有哪些方式可以获得`Linux`的实验环境❓

方式有很多，大致方式有以下：

1. 本地方式
    1. 祼机安装`Linux`
    1. 虚拟机安装`Linux`
    1. `WIndows Subsystem for Linux`（`Windows10`以上版本）
    1. `Docker`启动`Linux`
1. 可移动方式
    1. `LiveUSB`启动（可在搜索引擎中搜索`ubuntu live usb 制作`）
    1. 🌟 虚拟机迁移
    1. [`Play With Docker`](https://labs.play-with-docker.com/)（<https://labs.play-with-docker.com/>）提供的免费的有效期4小时的`Linux`容器实例（ **需要`docker账号`登录** ）

⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️

1. `LiveUSB`
    1. 需要设置`BIOS`允许`USB`启动
    1. 可能需要关闭`BIOS`的`security boot`
    1. 如果`USB`不是第一启动顺序，开机时需要按下按键以便从`USB`启动，根据不同的机器相应按键不同，如：`联想台式机`为按`<ENTER>`后按`<F12>`
1. `Play With Docker`
    1. 需要`docker账号`登录后才能使用
    1. 免费的`Linux`容器实例有效期4小时（从生成容器实例起计算）
    1. 由于需要网络传输，有较长的延时

⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️

## `VirtualBox`如何进行虚拟机迁移❓

可使用`VirtualBox`的`Export`功能导出虚拟机（选择`Open Virtualization Format 1.0`格式即可），然后在另一台机器的`VirtualBox`导入该虚拟机。

下图以`macOS`的`VirtualBox`导出截图为例（`Windows`的`VirtualBox`类似）。

![export_to_oci](./.assets/image/export_to_oci.png)
