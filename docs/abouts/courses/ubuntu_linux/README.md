---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# About This Course

---

## Basic Information

1. 课程名称：Linux系统应用
1. 课程编码：0822292
1. 课程类型：专业课程
1. 课程性质：专业选修
1. 适用范围：计算机科学与技术、软件工程、数据科学与大数据技术
1. 课程学分：2
1. 先修课程：《计算机组成原理》或《计算系统基础》、《操作系统》

---

1. 课程学时：36
1. 实验/实践学时：18
1. 课外学时：0
1. 考核方式：考查（平时40%+期末60%）
1. 后续课程：《软件工程综合实践》、《Linux应用开发》等

---

## Goals

1. 理解`Operating System Principle`与本课程的关系
1. 了解`UNIX-like`、`GNU/Linux`的历史及重要人物
1. 理解`UNIX-like`与`Windows`的联系与区别
1. 掌握`GNU/Linux`的基本使用、基本配置与管理、基本命令
1. 掌握`Shell`的基本编程
1. 掌握`GNU/Linux`的基本运维技能
1. 了解`GNU/Linux`的开发环境

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
