---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Backup_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Backup

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [数据备份：内容、方式和规划](#数据备份内容-方式和规划)
2. [使用存档工具进行备份](#使用存档工具进行备份)
3. [使用`dump`进行备份和恢复](#使用dump进行备份和恢复)
4. [使用光盘进行备份](#使用光盘进行备份)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据备份：内容、方式和规划

---

### 备份的内容

1. 系统备份：操作系统和应用程序的备份，目的是系统崩溃后能快速、简单、完整地恢复系统的运行
1. 用户备份：用户数据的备份，目的是系统崩溃或用户误操作后能快速、简单、完整地恢复用户数据

---

### 备份的方式

1. `full backup`完全备份：全面的、完整的备份
1. `incremental backup`增量备份：对上一次备份后增加的和修改的内容进行备份
1. `differential backup`差异备份：对上一次完全备份后增加的和修改的内容进行备份

---

### 备份的规划

1. 单纯的完全备份
1. 完全备份+差异备份

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 使用存档工具进行备份

---

```bash {.line-numbers}
tar (1)              - an archiving utility
tar -czvf
tar -xzvf

dd (1)               - convert and copy a file
dd if=<FILE> of=<FILE>
dd if=/dev/sda2 of=/tmp/xx_bak.iso
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 使用`dump`进行备份和恢复

---

```bash {.line-numbers}
apt install dump

apt show dump
#  backup and restore for ext2/3/4 filesystems

dump (8)             - ext2/3/4 filesystem backup
restore (8)          - restore files or file systems from backups made with dump
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 使用光盘进行备份

---

```bash {.line-numbers}
apt install mkisofs || apt install genisoimage

mkisofs (1)          - create ISO9660/Joliet/HFS filesystem with optional Rock Ridge attributes

apt install cdrecord || apt install wodim

cdrecord (1)         - write data to optical disk media
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
