---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Storage Management_"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Storage Management存储管理

---

## Contents

1. :star2: storage overview存储概述：设备文件、磁盘分区、文件系统
1. disk partition and mount硬盘分区与挂载
1. :star2: logical volume manager逻辑卷管理
1. mount and use extern storage外部存储设备挂载和使用
1. data backup数据备份

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
