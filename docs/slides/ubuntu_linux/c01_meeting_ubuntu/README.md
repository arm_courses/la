# Meeting Ubuntu遇见Ubuntu

## Contents

```sh {.line-numbers}
.
├── s01_install_linux
├── s02_brief_history
├── s03_distro
├── s04_operating_env
└── s05_programming_env
```

1. `Linux`的安装
1. `Linux`简史：`UNIX`，`UNIX-like`，`POSIX`，`SUS`，`MINIX`，`GNU/Linux`
1. `Linux`发行版:star2: `Linux Package Manager` and `Linux Distribution`
1. `Linux`使用环境
    1. :star2: `CLI`, `GUI` and `shell`
    1. `dpkg`, `apt` and `snap`
1. `Linux`编程环境
    1. 编辑器`vim`，`emacs`与`nano`
    1. 编译器`GCC`与调试器`GDB`
    1. 自动化构建工具`make`
    1. 自动化构建工具集`GNU Autotools`
    1. `GUI frameworks`: `GTK` and `Qt`
