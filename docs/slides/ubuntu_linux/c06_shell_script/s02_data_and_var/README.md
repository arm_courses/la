---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Data and Variable_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Data and Variable

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [数据类型](#数据类型)
2. [变量名与变量类型](#变量名与变量类型)
3. [变量的`CRUD`](#变量的crud)
4. [特殊变量/魔术变量](#特殊变量魔术变量)
5. [变量的`std`输入输出](#变量的std输入输出)
6. [字符串数据类型](#字符串数据类型)
7. [数组数据类型](#数组数据类型)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据类型

---

1. 字符型
1. 数组

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 变量名与变量类型

---

### 变量名

1. 字母、数字、下划线
1. 首字符必须是字母
1. 不能使用保留字/关键字

---

### 变量的左值与右值

1. 左值：不带`$`符，变量名
1. 右值：带`$`符，变量值

---

### 变量的类型---按变量类型分类

>Unlike many other programming languages, Bash does not segregate its variables by "type." Essentially, Bash variables are **_character strings_**, but, depending on context, Bash permits arithmetic operations and comparisons on variables. The determining factor is whether the value of a variable contains only digits.
>>[ABS. 4.3. Bash Variables Are Untyped](https://tldp.org/LDP/abs/html/untyped.html)

---

### 变量的类型---按作用域分类

1. 自定义变量
    1. 局部变量/本地变量：本`function`可见（使用`local`声明）
    1. 全局变量：本`shell`可见
1. 环境变量：`shell`环境的一部分，`shell`及`child shell`共享

:warning: 有些书籍将`environment variable`称为`global environment variable`、将`global variable`称为`local environment variable` :warning:

<!--
❗ 一般情况下，环境变量名均由`shell`定义，因此，也称为`shell variables` ：❗
-->
---

#### 特殊变量/`shell parameters`

1. `positional parameters`位置参数
1. `special parameters`特殊参数

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 变量的`CRUD`

---

### 变量的Create---定义和声明

1. 赋值`var_name=value`：等号两边不能有空格（否则会将`var_name`识别为`command`）
1. 变量替换
1. `declare`声明/`typeset`声明：`typeset` is a synonym for `declare`
1. `readonly`声明：等同于`declare -r`声明，`readonly`变量不能被`unset`
1. `local`声明：声明变量为`block`级局部变量，`declare`用于函数中时等同于`local`声明
1. `let`

---

#### `declare`声明

```sh {.line-numbers}
-a  to make NAMEs indexed arrays (if supported)
-A  to make NAMEs associative arrays (if supported)
-i  to make NAMEs have the `integer' attribute
-l  to convert the value of each NAME to lower case on assignment
-n  make NAME a reference to the variable named by its value
-r  to make NAMEs readonly
-t  to make NAMEs have the `trace' attribute
-u  to convert the value of each NAME to upper case on assignment
-x  to make NAMEs export
```

❗ `declare -i`功能有限，仅支持最基本的算术运算（加减乘除和取余），不支持逻辑运算 ❗

---

```sh {.line-numbers}
-f  restrict action or display to function names and definitions
-F  restrict display to function names only (plus line number and
    source file when debugging)
-g  create global variables when used in a shell function; otherwise
    ignored
-p  display the attributes and value of each NAME
```

❗ `declare -g`用于函数中时将变量定义为全局变量，在非函数中时忽略该选项 :exclamation:

---

1. `declare` == `typeset`
1. `declare -r` == `readonly`
1. `declare -x` == `export`
1. `declare` in `block` == `local`
1. `declare -g` in `block` == empty declaration

---

### 变量的Create---导出

1. `export`：导出变量到`child shell`

⚠️ `child shell`修改继承的环境变量，不会影响到`father shell` ⚠️

---

### 变量的Retrieve

1. `${<var_name>}`
1. `${!<var_name>}`：`var_name`的值本身是一个变量名

---

1. 环境变量
    1. `env`
    1. `printenv`
    1. `export`
1. 环境变量+自定义变量+`function`
    1. `set`
    1. `declare`

---

### 变量的Delete

1. `unset`：`bash`中不存的变量等于空字符串
    1. `<var_name>=''`：等号右边为空字符串
    1. `<var_name>=`：等号右边不写任何值
1. `env -u, --unset=<var_name>`：remove variable from the environment

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 特殊变量/魔术变量

---

1. `$?`：上一个命令的退出码，`0`表示执行成功，`非0`表示执行失败
1. `$$`：当前`shell`的`process id`
1. `$-`：当前`shell`的启动参数形成的字符串
1. `$_`：上一命令的最后一个参数
1. `$!`：最近一个后台执行`job`的`process id`

---

1. `$0`：当前`shell`（交互式命令行）或`rc`（脚本运行）的名称
1. `$n`：位置参数
1. `$#`：参数的数量
1. `$*`：所有参数的值，`"$*"`是一个整体
1. `$@`：所有参数的值，`"$@"`是用空格分开的各个值

👉 see more in [addons/bash](../../../../addons/bash/README.md)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 变量的`std`输入输出

---

### 变量的`stdin`输入

1. `read`

---

### 变量的`stdout`输出

1. `echo`
1. `printf`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字符串数据类型

---

### 字符串字面量

1. 被单引号包围
    + 原样输出：不进行变量替换`$`，不进行命令替换`` ` ``，不进行反斜杠转义`\`
    + 不能出现单引号（转义单引号也无效），可以出现双引号
1. 被双引号包围
    + 进行变量替换，进行命令替换，进行反斜杠转义
    + 可以出现单引号，可以出现双引号（需转义）
1. 不被引号包围
    + 进行变量替换，进行命令替换，进行反斜杠转义
    + 可以出现单引号（需转义），可以出现双引号（需转义）
    + 不能出现空白字符，空白字符后的字符串作为其他变量或命令进行解析

---

### 字符串操作

```bash {.line-numbers}
${var_name_01}${var_name_02}other_str       # 字符串拼接
```

---

### 字符串操作---变量替换/`Parameter Expansion`参数扩展

```bash {.line-numbers}
${var_name}                                 # 字符串
${#var_name}                                # 字符串长度
${var_name:offset:length}                   # 子符串
${var_name^^}                               # 改写成大写
${var_name,,}                               # 改写为小写
```

---

```bash {.line-numbers}
${var_name:-word}   # 如果var_name不存在或为空，则返回word，但不改变var_name，否则，返回var_name的值
${var_name:=word}   # 如果var_name不存在或为空，则返回word，且将var_name的值设置为word，否则，返回var_name的值
${var_name:?msg}    # 如果var_name不存在或为空，则将msg发送到stderr，同时，在rc中将中止运行，否则，返回var_name的值
${var_name:+word}   # 如果var_name存在且不为空，则返回word，但不改变var_name，否则返回空
```

---

```bash {.line-numbers}
${var_name#pattern}     # 头部模式匹配，删除最短匹配（非贪婪匹配）的部分，返回剩余部分
${var_name##pattern}    # 头部模式匹配，删除最长匹配（贪婪匹配）的部分，返回剩余部分
${var_name%pattern}     # 尾部模式匹配，删除最短匹配（非贪婪匹配）的部分，返回剩余部分
${var_name%%pattern}    # 尾部模式匹配，删除最长匹配（贪婪匹配）的部分，返回剩余部分
```

---

```bash {.line-numbers}
${variable/pattern/string}  #任意位置模式匹配`pattern`并替换成`string`
```

<!--TODO:
not finish here...
-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数组数据类型

>[阮一峰. Bash脚本教程-数组.](https://wangdoc.com/bash/array.html)

---

### 数组的类型

1. `indexed array`索引数组
1. `associative array`关联数组

---

### 数组的定义

```sh {.line-numbers}
# approach one
declare -a <array_name>     # indexed array
declare -A <array_name>     # associative array

# approach two
<array_name>=(<elem_1>␣...␣<elem_n>)    # indexed array

# approach three
<array_name>[0]=<elem_1>
...
<array_name>[n]=<elem_n>

<array_name>["<name_1>"]=<elem_1>
...
<array_name>["<name_n>"]=<elem_n>
```

---

### 数组的使用

```sh {.line-numbers}
${<array_name>[*]}
${<array_name>[@]}

# length of array
${#<array_name>[@]}

# length of element in string type
${#<array_name>[n]}
${#<array_name>["<name_n>"]}

# value of element
${<array_name>[n]}
${<array_name>["<name_n>"]}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
