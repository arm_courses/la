# Shell Script脚本编程

## Goals

## Contents

```sh {.line-numbers}
.
├── s01_rc_env
├── s02_data_and_var
├── s03_cal_and_expr
├── s04_flow_ctrl
└── s05_func_and_file
```

<!--

1. :star2:  environment结构与环境
1. 字面量、常量和变量
1. :star2: data type, data structure and variable数据类型与数据结构
1. :star2: statement, expression and operator语句、表达式与运算符
1. :star2: determine and flow control判定与流程控制
1. input, output and user interface
1. :star2: modular模块化
1. 程序协作
-->
