---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Bash Script Environment_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Bash Script Environment

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Running Environment and Script Concepts](#running-environment-and-script-concepts)
2. [Coding Environment](#coding-environment)
3. [Debugging Environment](#debugging-environment)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Running Environment and Script Concepts

---

1. `rc`文件：`rc(run command)`是脚本类文件的约定俗成的后缀（非扩展名）
1. `shell script`是使用最广泛的胶水语言，语言本身功能不多、能完成的任务不多，主要通过调用各种命令和可执行文件完成任务，是一系列命令的集合
1. `shell`即是交互式命令工具，也是非交互式脚本语言，因此需同时满足两种运行环境
1. `command`和`function`的输出和返回值是不相同的，输出指的是输出到`stdout`的结果，返回值是命令的`return`值，赋值给变量的值是输出值、而不是返回值 

---

![height:560](./.assets/image/bash_execution_process.png)

>[SHELL(bash)脚本编程六：执行流程](https://segmentfault.com/a/1190000008215772)

---

### 运行`rc`的方式

>[TinyLab. 程序执行的一刹那.](https://tinylab-1.gitbook.io/cbook/02-chapter3)

1. 直接运行有“可执行权限”的`rc`：`<rc_path>`（⚠️ 当前目录一般不在`${PATH}`中 ⚠️）
1. 指定`shell`命令执行：`<shell_name> <rc_file> [arguments]...`
1. 重定向到指定`shell命令`：`<shell_name> < <rc_file>`（不能传递选项和参数给`rc`）

---

### `set` and `shopt`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Coding Environment

---

### IDE

1. `vi/vim`
1. `vscode`
    + `shellman`：智能提示、自动补全
    + `shell-check`：语法错误检查
    + `shell-format`：源代码格式化
    + `Code Runner`：运行

---

1. `JetBrains`
    1. `Shell Script`(bundled)
        1. Coding assistance: code completion, quick documentation, code refactorings, etc.
    1. Dedicated run/debug configuration for shell scripts
    1. Integration with external tools (`ShellCheck`, `Shfmt`, `Explainshell`)

---

### `shell script`的基本构成和原理

```sh {.line-numbers}
#!/usr/bin/env bash

# this is a comment
<commands>
```

1. 第1行以`#!`开头，指定运行`script`的解释器（非必须，但建议）
    + 使用`env`（必定在`/usr/bin`目录下）返回指定`shell`的路径
    + 仅在直接执行`rc`时有效
1. 以`#`开头的行是注释行
1. 从文件顶部开始依次执行

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Debugging Environment

>1. [tinylab. BASH的调试手段.](http://tinylab.org/bash-debugging-tools/)

---

```sh {.line-numbers}
-v      Print shell input lines as they are read.
-x      Print commands and their arguments as they are executed.
```

>`man bash`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
