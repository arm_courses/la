---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Calculate and Expression_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Calculate and Expression

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Conditional Judgment条件判断](#conditional-judgment条件判断)
2. [Arithmetic Expressions](#arithmetic-expressions)
3. [Conditional Expressions](#conditional-expressions)
4. [其他计算命令](#其他计算命令)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Conditional Judgment条件判断

---

1. 使用命令的返回值，返回值为`0`代表`true`，返回值非`0`代表`false`

---

### `POSIX`标准

1. `test`/`[`（`builtin command`）：`conditional expression`

---

### `bash` Features

1. `[[`（`keyword`）：`conditional expression`
1. `let`/`((`（`Arithmetic Expansion`）：赋值运算表达式、算术运算表达式、关系运算表达式、布尔运算表达式、按位运算表达式

---

1. `[[`和`((`是`bash`的扩充，不是`POSIX`标准，`sh`也不支持
1. `((`的表达式对空白字符要求不严格，`let`的表达式对空白字符要求严格（不能有空白字符，部分特殊字符需要转义），因此，**推荐优先使用`((`**
1. `/usr/bin/expr`（`external command`）只支持算术表达式、关系表达式、逻辑表达式，且对空白字符要求严格（不能没有空白字符，部分特殊字符需要转义），**不推荐使用**

---

>`((expression))`
>The _`arithmetic expression`_ is evaluated according to the rules described below. If the value of the expression is non-zero, the return status is 0; otherwise the return status is 1. This is exactly equivalent to
>`let "expression"`
>>[Bash Reference Manual. 3.2.5.2 Conditional Constructs.](https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html)

---

>`[[ expression ]]`
>Return a status of 0 or 1 depending on the evaluation of the _`conditional expression`_.
>>[Bash Reference Manual. 3.2.5.2 Conditional Constructs.](https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html)

---

```sh {.line-numbers}
$ echo $?
0
$ [[ 1 > 2 ]]
$ echo $?
1
$ [[ 1 < 2 ]]
0
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Arithmetic Expressions

---

>The shell allows arithmetic expressions to be evaluated, as one of the shell expansions or by using the `((` compound command, the `let` builtin, or the `-i` option to the `declare` builtin.
>>[Bash Reference Manual. 6.5 Shell Arithmetic](https://www.gnu.org/software/bash/manual/html_node/Shell-Arithmetic.html)

1. 算术运算符、关系运算符、赋值运算符、逻辑运算符、按位运算符、其他运算符
1. `((`/`$(())`，`let`/`$(let)`
1. `$[]`与`$(())`等同，`$[]`是过去形式，现已不建议使用

---

1. `help let`
1. [Bash Reference Manual. 6.5 Shell Arithmetic](https://www.gnu.org/software/bash/manual/html_node/Shell-Arithmetic.html)

❗ `expr`, `(())/let`只能进行整型运算，不能进行浮点运算 ❗

---

```sh {.line-numbers}
The levels are listed in order of decreasing precedence.

id++, id--      variable post-increment, post-decrement
++id, --id      variable pre-increment, pre-decrement
-, +            unary minus, plus
!, ~            logical and bitwise negation
**              exponentiation
*, /, %         multiplication, division, remainder
+, -            addition, subtraction
<<, >>          left and right bitwise shifts
<=, >=, <, >    comparison
==, !=          equality, inequality
&               bitwise AND
^               bitwise XOR
|               bitwise OR
&&              logical AND
||              logical OR
expr ? expr : expr
                conditional operator
=, *=, /=, %=,
+=, -=, <<=, >>=,
&=, ^=, |=      assignment
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Conditional Expressions

---

>Conditional expressions are used by the `[[` compound command and the `test` and `[` builtin commands.
>When used with `[[`, the `‘<’` and `‘>’` operators sort lexicographically using the current locale. The `test` command uses ASCII ordering.

---

1. integer operators：整数关系运算符
1. string operators：字符串关系运算符、字符串检测运算符
1. file operators
1. other operators

⚠️⚠️⚠️⚠️⚠️⚠️

1. `==`/`!=`/`<`/`>`在`conditional expression`中按字符串比较
1. 数值比较使用`-eq`/`-ne`/`gt`/`ge`/`lt`/`le`

⚠️⚠️⚠️⚠️⚠️⚠️

---

```sh {.line-numbers}
other operators:

-o OPTION           True if the shell option OPTION is enabled.
-v VAR              True if the shell variable VAR is set.
-R VAR              True if the shell variable VAR is set and is a name reference.
! EXPR              True if expr is false.
EXPR1 -a EXPR2      True if both expr1 AND expr2 are true.
EXPR1 -o EXPR2      True if either expr1 OR expr2 is true.
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 其他计算命令

---

1. `bc`
1. `awk`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
