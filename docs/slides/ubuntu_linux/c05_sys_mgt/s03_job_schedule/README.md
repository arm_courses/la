---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Job Schedule_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Job Schedule

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [使用`cron`服务安排周期性任务](#使用cron服务安排周期性任务)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 使用`cron`服务安排周期性任务

---

1. `cron`：用于管理周期性、重复性的作业任务的调度

```bash {.line-numbers}
cron (8)             - daemon to execute scheduled commands (Vixie Cron)
run-parts (8)        - run scripts or programs in a directory
```

---

```bash {.line-numbers}
drwxr-xr-x 2 root root       cron.d
drwxr-xr-x 2 root root       cron.daily
drwxr-xr-x 2 root root       cron.hourly
drwxr-xr-x 2 root root       cron.monthly
drwxr-xr-x 2 root root       cron.weekly
-rw-r--r-- 1 root root       crontab
```

---

```bash {.line-numbers}
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed
17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
```

---

### 使用`crontab`设置普通用户的任务调度

```bash {.line-numbers}
crontab (1)          - maintain crontab files for individual users (Vixie Cron)

crontab [ -u <user> ] [ -i ] { -e | -l | -r }
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
