---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Service Management_"
footer: "@aRoming"
math: katex
---


<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Service Management

>1. [通过 SysVinit、Systemd 和 Upstart 管理系统自启动进程和服务](https://linux.cn/article-7365-1.html)

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [系统初始化](#系统初始化)
2. [`systemd`的主要术语](#systemd的主要术语)
3. [`systemctl` Command](#systemctl-command)
4. [`systemd-analyze` Command](#systemd-analyze-command)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 系统初始化

---

1. `SysVinit(System V Initialization)`
1. `UpStart`
1. `systemd`

---

### `SysVinit`

1. 基于`RunLevel`对应的初始化脚本启动系统
    1. `/etc/inittab`
    1. `/etc/rc<n>.d/*`->`/etc/init.d/*`
1. 顺序线性启动机制

---

### `systemd`

1. 基于启动目标和单元文件启动系统
    1. `/etc/systemd/system/`
    1. `lib/systemd/system/`
1. 并行启动机制

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `systemd`

>1. [Arch Linux Wiki. systemd.](https://wiki.archlinux.org/title/Systemd)

---

```bash {.line-numbers}
systemd-boot — simple UEFI boot manager;
systemd-firstboot — basic system setting initialization before first boot;
systemd-homed — portable human-user accounts;
systemd-logind — session management;
systemd-networkd — network configuration management;
systemd-nspawn — light-weight namespace container;
systemd-resolved — network name resolution;
systemd-sysusers(8) — creates system users and groups and adds users to groups at package installation or boot time;
systemd-timesyncd — system time synchronization across the network;
systemd/Journal — system logging;
systemd/Timers — monotonic or realtime timers for controlling .service files or events, reasonable alternative to cron.
systemd-stub(7) — a UEFI boot stub used for creating unified kernel images.
```

---

![height:600](./.assets/image/Systemd_components.png)

---

![height:600](./.assets/image/Linux_kernel_unified_hierarchy_cgroups_and_systemd.png)

---

### `unit`

|单元类型|扩展名|说明|
|--|--|--|
|service|.service|定义系统服务。这是最常用的一类，与早期Linux版本/etc/init.d/目录下的服务脚本的作用相同
|device|.device|定义内核识别的设备。每一个使用udev规则标记的设备都会在systemd中作为一个设备单元出现
|mount|.mount|定义文件系统挂载点
|automount|.automount|用于文件系统自动挂载设备

---

|单元类型|扩展名|说明|
|--|--|--|
socket|.socket|定义系统和互联网中的一个套接字，标识进程间通信用到的socket文件
swap|.swap|标识管理用于交换空间的设备
path|.path|定义文件系统中的文件或目录
swap|.swap|标识管理用于交换空间的设备
timer|.timer|用来定时触发用户定义的操作，以取代atd、crond等传统的定时服务
target|.target|用于对其他单元进行逻辑分组，主要用于模拟实现运行级别的概念
snapshot|.snapshot|快照是一组配置单元，保存了系统当前的运行状态

---

### 依赖与事务

1. 在单元文件中使用`require <unit>`定义依赖
    1. `required`强依赖
    1. `wants`弱依赖
1. `systemd`的事务旨在保证多个依赖的单元间没有循环依赖
    1. 去除`wants`打破循环依赖

---

### `target`和`runlevel`

|`runlevel`|`target`|说明|
|--|--|--|
0|runlevel0.target，poweroff.target|关闭系统
1, s, single|runlevel1.target， rescue.target|单用户模式
2, 3, 4|runlevel2.target，runlevel3.target， runlevel4.target，multi-user.target|`CLI`多用户模式
5|runlevel5.target，graphical.target|`GUI`多用户模式

---

|`runlevel`|`target`|说明|
|--|--|--|
6|runlevel6.target，reboot.target|重启系统
Emergency|emergency.target|紧急Shell

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `systemctl` Command

>1. [阮一峰. Systemd 入门教程：命令篇.](https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-commands.html)
>1. [阮一峰. Systemd 入门教程：实战篇](https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-part-two.html)
>1. [Archlinux Wiki. systemd.](https://wiki.archlinux.org/title/systemd_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))

---

```bash {.line-numbers}
systemctl (1)        - Control the systemd system and service manager
```

---

### `systemctl`管理启动目标

```bash {.line-numbers}
systemctl isolate <target>

systemctl get-default

systemctl set-default <target>
```

---

### `systemctl`管理系统

```bash {.line-numbers}
systemctl reboot                        # 重启系统

systemctl poweroff                      # 关闭系统，切断电源

systemctl halt                          # CPU停止工作

systemctl suspend                       # 暂停系统

systemctl hibernate              # 进入冬眠状态

systemctl hybrid-sleep           # 进入交互式休眠状态

systemctl rescue                 # 进入救援状态（单用户状态）
```

---

### `systemctl`管理`service`开机启动

```bash {.line-numbers}
systemctl list-unit-files --type=service

systemctl is-enabled <service>

systemctl enable <service>

systemctl disable <service>

systemctl mask <service>                # 禁止指定服务设置为开机启动

systemctl unmask <service>              # 取消禁止指定服务设置为开机启动
```

---

### `systemctl`查询`unit`状态

```bash {.line-numbers}
systemctl list-units            # 列出正在运行的 Unit

systemctl status [<unit>]     # 显示unit状态

systemctl is-active <unit>

systemctl is-failed <unit>
```

---

### `systemctl`管理`unit`状态

```bash {.line-numbers}
systemctl start <unit>                  # 启动

systemctl stop <unit>                   # 停止

systemctl restart <unit>                # 重启

systemctl tryrestart <unit>             # 尝试重启

systemctl kill <unit>                   # 杀死一个unit进程及其子进程

systemctl reload <unit>                 # 重新加载配置文件

systemctl daemon-reload                 # 重载所有修改过的配置文件
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `systemd-analyze` Command

---

```bash {.line-numbers}
systemd-analyze                                 # 查看启动耗时

systemd-analyze blame                           # 查看每个服务的启动耗时 

systemd-analyze critical-chain                  # 显示瀑布状的启动过程流

systemd-analyze critical-chain <unit>           # 显示指定服务的启动流

systemd-analyze verify <unit_config_file>       # 检查单元文件的语法
```

<!--

## `unit`配置文件

---

1. `/etc/systemd/system/*`
1. `/lib/systemd/system/*`：
1. `/run/systemd/system/*`

-->

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
