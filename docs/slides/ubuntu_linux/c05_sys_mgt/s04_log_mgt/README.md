---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Log Management_"
footer: "@aRoming"
math: katex
---


<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Log Management

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`syslog`日志管理工具](#syslog日志管理工具)
2. [`systemd-journald`日志管理工具](#systemd-journald日志管理工具)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `syslog`日志管理工具

---

```bash {.line-numbers}
rsyslogd (8)         - reliable and extended syslogd

systemctl status rsyslog.service

less /etc/rsyslog.conf

tree /etc/rsyslog.d
/etc/rsyslog.d/
├── 20-ufw.conf
├── 21-cloudinit.conf
├── 50-default.conf
└── postfix.conf
```

---

```bash {.line-numbers}
vim /etc/rsyslog.conf

systemctl restart rsyslog

less /var/log/<log_file>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `systemd-journald`日志管理工具

---

```bash {.line-numbers}
systemd-journald (8)            - Journal service

journalctl (1)                  - Query the systemd journal

less /etc/systemd/journald.conf
```

---

```bash {.line-numbers}
systemctl status systemd-journald.service

journalctl -n, --lines=

journalctl -f, --follow

journalctl -p err

journalctl -k

journalctl /lib/systemd/systemd

journalctl -u <unit>

journalctl --disk-usage
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
