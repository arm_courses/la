---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_User and Group Management_"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# User and Group Management

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`user` Management](#user-management)
2. [`group` Management](#group-management)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `user` Management

---

### `user` Read

1. `cat /etc/passwd | grep <username>`
1. `id`
1. `w`/`who`/`whoami`/`who am i`

---

### `user` Create

1. `useradd`
1. `adduser`

:point_right: `useradd`默认不创建`user home directory` :point_left:

>useradd is a low level utility for adding users. On Debian, administrators should usually use adduser(8) instead.
>
>`man useradd`

---

### `user` Update

1. `passwd`
1. `usermod`
    1. `usermod -g <group_name> [<username>]`：设置用户的主组
    1. `usermod -aG <group_name>... [<username>]`：添加用户到组

⚠️ `usermod -G <group_name>... [<username>]`会将用户从未列到命令中的组中移出 ⚠️

---

### `user` Delete

1. `userdel`
1. `deluser`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `group` Management

---

### `group` Read

1. `groups`

---

### `group` Create

1. `groupadd`
1. `addgroup`

---

### `group` Update

1. `gpasswd`
1. `groupmod`

---

### `group` Delete

1. `groupdel`
1. `delgroup`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

**:sparkles: :sparkles: :sparkles: Thank You :sparkles: :sparkles: :sparkles:**

**:ok: End of This Slide :ok:**
