---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Filesystem Hierarchy Standard_"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Filesystem Hierarchy Standard文件系统层次标准

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [目录树](#目录树)
2. [`FHS(Filesystem Hierarchy Standard)`](#fhsfilesystem-hierarchy-standard)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 目录树

---

### 结构与路径

1. 起点为根结点（`/`）的单根树结构
1. 目录可以是本地分区的文件系统，也可以是网络上的文件系统
1. 绝对路径独一无二
    + 绝对路径：从`/`开始的路径
    + 相对路径：从`wd`开始的路径

---

### 文件命名规范

<!--TODO:
find the <limits.h> location
-->

1. 长度限制
    1. 文件名长度255个字符
    1. 路径长度4096个字符
1. 不可包含`/`字符
1. 避免文件名仅大小写不同
1. 建议同类文件使用相同的后缀

---

1. 避免使用以下字符
    1. 空白字符：空格符、制表符、回车符
    1. 通配字符：星号、问号
    1. 重定向字符：大于号、小于号
    1. 分隔符：冒号、分号
    1. 括号：圆括号、方括号、花括号
    1. 引号：单引号、双引号、反引号

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `FHS(Filesystem Hierarchy Standard)`

---

1. 规范内容：
    1. 第一层目录规范：`/`下各目录
    1. 第二层目录规范：`/usr`和`/var`下的各子目录
1. [FHS3.0(2015)](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)

---

![height:600](./.assets/image/linux-file-system-structure_in_brief.png)

---

>1. [Differences between /bin, /sbin, /usr/bin, /usr/sbin, /usr/local/bin, /usr/local/sbin](https://askubuntu.com/questions/308045/differences-between-bin-sbin-usr-bin-usr-sbin-usr-local-bin-usr-local)
>1. [Where should I put my bash scripts](https://askubuntu.com/questions/998452/where-should-i-put-my-bash-scripts)

---

### `FHS` History目录结构历史

>1. `/`：存放系统程序，也就是`AT&T`开发的`UNIX`程序
>1. `/usr`：存放`UNIX`系统商（比如`IBM`和`HP`）开发的程序
>1. `/usr/local`：存放用户自己安装的程序
>1. `/opt`：在某些系统，用于存放第三方厂商开发的程序，所以取名为option，意为"选装"
>
>>[阮一峰. Unix目录结构的来历.](http://www.ruanyifeng.com/blog/2012/02/a_history_of_unix_directory_structure.html)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

**:sparkles: :sparkles: :sparkles: Thank You :sparkles: :sparkles: :sparkles:**

**:ok: End of This Slide :ok:**
