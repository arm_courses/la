# File Management文件管理

## Goals

1. 熟悉`Linux`的目录结构
1. 理解`Linux`的文件类型
1. 掌握文件和目录的`CLI`操作方法

## Contents

```sh {.line-numbers}
.
├── s01_fhs
├── s02_file_type
└── s03_file_mgt
```

1. :star2: `FHS(Filesystem Hierarchy Standard, 文件系统层次结构标准)`
1. :star2: file type文件类型
1. :star2: file management文件管理
