#!/usr/bin/env bash

file="/dev/sda"

echo -n "${file} "

if [ ! -e ${file} ]; then
    echo "is not exist..."
elif [ -f ${file} ]; then
    echo "is a regular file..."
elif [ -d ${file} ]; then
    echo "is a directory..."
elif [ -h ${file} ]; then
    echo "is a symbolic link..."
elif [ -b ${file} ]; then
    echo "is a block device..."
elif [ -c ${file} ]; then
    echo "is a character device..."
elif [ -p ${file} ]; then
    echo "is a pip"
elif [ -S ${file} ]; then
    echo "is a socket"
fi
