#!/usr/bin/env bash

sum=0
for i in $(seq 1 100); do
    ((sum += i))
done
echo "for in loop ==> sum is ${sum}"

sum=0
for ((i = 1; i <= 100; ++i)); do
    ((sum += i))
done

echo "c-style for loop ==> sum is ${sum}"

sum=0
i=1
while ((i <= 100)); do
    ((sum += i))
    ((++i))
done
echo "while loop ==> sum is ${sum}"

sum=0
i=1
until ((i > 100)); do
    ((sum += i))
    ((++i))
done
echo "until loop ==> sum is ${sum}"
