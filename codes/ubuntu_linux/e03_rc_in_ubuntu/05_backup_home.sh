#!/usr/bin/env bash

tar -zcvf mybackup.tar.gz /home

# add into /etc/crontab (without the first '#')
# 0 0 * * * root <current_rc_full_path>

# or copy into /etc/cron.daily/