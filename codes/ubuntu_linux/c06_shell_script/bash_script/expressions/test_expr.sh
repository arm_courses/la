#!/usr/bin/env bash

source ./echo_utils.sh

function demo_parentheses() {
    i=10
    if ((i%=3)); then
        echo_true
    else
        echo_false
    fi

    if ((i=10));then
        echo_true
    else
        echo_false
    fi

    if ((10000+2));then
        echo_true
    else
        echo_false
    fi

    if ((0000));then
        echo_true
    else
        echo_false
    fi

    if ((1<2));then
        echo_true
    else
        echo_false
    fi

    if ((1&&2));then
        echo_true
    else
        echo_false
    fi

    if ((10>>1));then
        echo_true
        echo $?
    else
        echo_false
        echo $?
    fi

    if (ll);then
        echo_true
        echo $?
    else
        echo_false
        echo $?
    fi

    if ret_non_zero;then
        echo_true
        echo $?
    else
        echo_false
        echo $?
    fi

    if let 1+2; then
        echo_true
    else
        echo_false
    fi
}

function ret_non_zero() {

    return 255
}

demo_parentheses
