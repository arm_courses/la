#!/usr/bin/env bash

global_one="global one..."

function demo_var_local() {
    local local_one="function local one use declare..."
    declare local_two="function local two use declare..."
    typeset local_three="function local three use typeset..."
    global_two="function global two use nothing..."
    declare -g global_three="function global three use declare -r..."
    if ((1)); then
        local local_four="if compound in function local four use local..."
    fi
    {
        lcoal local_five="block in function local five use local..."
    }
    echo "inside the function..."
    echo "local_one==>$local_one"
    echo "local_two==>$local_two"
    echo "local_three==>$local_three"
    echo "local_four==>$local_four"
    echo "local_five==>$local_five"
}

echo -e "\nbefore call function..."

demo_var_local

echo -e "\nafter the function..."
echo "local_one==>$local_one"
echo "local_two==>$local_two"
echo "local_three==>$local_three"
echo "local_four==>$local_four"
echo "global_one==>$global_one"
echo "global_two==>$global_two"
echo "global_three==>$global_three"
