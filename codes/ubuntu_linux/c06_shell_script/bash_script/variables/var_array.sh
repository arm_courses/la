#!/usr/bin/env bash

function demo_array() {
    activities=(swimming "water skiing" canoeing "white-water rafting" surfing)
    local i
    echo -e '\n${<array_name>[*]} without quoting...'
    i=1
    for activity in ${activities[*]}; do
        echo "activity$((i++))==>${activity}"
    done

    i=1
    echo -e '\n${<array_name>[*]} with quoting...'
    for activity in "${activities[*]}"; do
        echo "activity$((i++))==>${activity}"
    done

    i=1
    echo -e '\n${<array_name>[@]} without quoting...'
    for activity in ${activities[@]}; do
        echo "activity$((i++))==>${activity}"
    done

    i=1
    echo -e '\n${<array_name>[@]} with quoting...'
    for activity in "${activities[@]}"; do
        echo "activity$((i++))==>${activity}"
    done
}

demo_array