#!/usr/bin/env bash

export USER_DEFINED_ENV_VAR=123

bash ./env_var_sub.sh

echo "child shell has unsetted USER_DEFINED_ENV_VAR..."

echo "parent shell's USER_DEFINED_ENV_VAR now is ==> ${USER_DEFINED_ENV_VAR}"