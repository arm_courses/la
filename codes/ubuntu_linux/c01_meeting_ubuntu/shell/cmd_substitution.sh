#!/usr/bin/env bash

echo '$(who | wc -l) tells how many users are logged in...'
echo "$(who | wc -l) tells how many users are logged in..."
echo '`who | wc -l ` tells how many users are logged in...'
echo "`who | wc -l` tells how many users are logged in..."

echo

echo `echo \\`
echo `echo \\\\`
echo $(echo \\)
echo $(echo \\\\)

echo

name="ken"
echo "hello $(echo $name)"

echo

x='abc def'
y='$x'
z='$y'
echo $y
$(echo $y)
`echo $y`
eval echo $x
eval echo $y
eval echo $z