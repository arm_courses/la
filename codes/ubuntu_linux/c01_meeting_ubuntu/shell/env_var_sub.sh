#!/usr/bin/env bash

echo "in child shell..."

echo "USER_DEFINED_ENV_VAR is $USER_DEFINED_ENV_VAR"

unset USER_DEFINED_ENV_VAR

echo "USER_DEFINED_ENV_VAR is unseted..."

echo "USER_DEFINED_ENV_VAR is now ==> ${USER_DEFINED_ENV_VAR}"